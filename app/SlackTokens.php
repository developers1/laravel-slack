<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SlackTokens
 * @package App
 */
class SlackTokens extends Model
{
    /**
     * @var string
     */
    protected $table = 'slack_tokens';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $guarded = ['id'];
    /**
     * @var bool
     */
    public $timestamps = true;
}
