<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

/**
 * Class SetSlackToken
 * @package App\Http\Middleware
 */
class SetSlackToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $repository = app()->make('App\Repositories\SlackRepository');
            $slackToken = $repository->getToken();
            // This is needed for "Socialite" to authenticate User.
            Config::set('services.slack.token', $slackToken);
            // This is needed for "SlackApi" to authenticate User.
            Config::set('slack.token', $slackToken);
        }
        return $next($request);
    }
}
