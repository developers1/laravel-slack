<?php

namespace App\Http\Controllers;

use Socialite;
use SlackApi;
use Lisennk\Laravel\SlackWebApi\Exceptions\SlackApiException;
use App\Repositories\SlackRepository;
use App\Http\Requests\MessagePost;
use App\Services\Slack\Slack;

/**
 * Class SlackController
 * @package App\Http\Controllers
 */
class SlackController extends Controller
{
    /**
     * @var SlackRepository
     */
    protected $repository;
    /**
     * @var Slack
     */
    protected $slackService;
    /**
     * @var bool
     */
    protected $token;

    /**
     * SlackController constructor.
     * @param SlackRepository $repository
     * @param Slack $slackService
     */
    public function __construct(SlackRepository $repository, Slack $slackService)
    {
        $this->repository   = $repository;
        $this->slackService = $slackService;
        $this->token        = $this->repository->getToken();
    }

    /**
     * Slack Login.
     * @return mixed
     */
    public function auth()
    {
        return Socialite::driver('slack')->scopes(['identity.basic'])->redirect();
    }

    /**
     * Slack Login callback.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authCallback()
    {
        $user = Socialite::driver('slack')->user();
        $this->repository->storeToken($user->token);
        flash('Successfully logged in Slack!!!')->success();
        return redirect()->route('slack.index');
    }

    /**
     * Index/Dashboard.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('slack.index');
    }

    /**
     * Open the Direct Message page with specific Slack User.
     * @param $userId
     * @return $this
     */
    public function message($userId)
    {
        $messageHistory = false;

        /*
         * Get information about User whom sending message.
         * Ref: https://api.slack.com/methods/users.info
         */
        $receiver = SlackApi::execute('users.info', [
            'token' => $this->token,
            'user'  => $userId
        ]);
        $receiver = $receiver['user'];

        try {
            /*
             * Get a list of all im channels that the user has.
             * Ref: https://api.slack.com/methods/im.list
             */
            $imList = SlackApi::execute('im.list', [
                'token' => $this->token
            ]);
            if ($imList) {
                foreach ($imList['ims'] as $im) {
                    if ($im['user'] == $userId) {
                        $imChannelId = $im['id'];

                        /*
                         * Get a portion of messages/events from the specified direct message channel.
                         * Ref: https://api.slack.com/methods/im.history
                         */
                        $originalMessageHistory     = SlackApi::execute('im.history', [
                            'token'     => $this->token,
                            'channel'   => $imChannelId,
                            'inclusive' => false,
                            'count'     => 10,
                        ]);
                        $messageHistory['has_more'] = $originalMessageHistory['has_more'];
                        $messageHistory['messages'] = $this->slackService->prepareDirectMessageHistory(app('slackData')['slackUser'], $receiver, $originalMessageHistory);
                        break;
                    }
                }
            }
        } catch (SlackApiException $e) {
        }

        return view('slack.message')->with(compact('userId', 'imChannelId', 'receiver', 'messageHistory'));
    }

    /**
     * Load Direct Message History via ajax.
     * @param $userId
     * @param $imChannelId
     * @param string $latestMessageTS
     * @param bool $latestSingleMessage
     * @return $this
     */
    public function imMessageHistory($userId, $imChannelId, $latestMessageTS = '', $latestSingleMessage = false)
    {
        $messageHistory = false;

        try {
            /*
             * Get information about User whom sending message.
             * Ref: https://api.slack.com/methods/users.info
             */
            $receiver = SlackApi::execute('users.info', [
                'token' => $this->token,
                'user'  => $userId
            ]);
            $receiver = $receiver['user'];

            /*
             * Get a portion of messages/events from the specified direct message channel.
             * Ref: https://api.slack.com/methods/im.history
             */
            $inputParams = [
                'token'     => $this->token,
                'channel'   => $imChannelId,
                'inclusive' => false,
                'count'     => 10,
            ];
            if ($latestMessageTS) {
                $inputParams['latest'] = $latestMessageTS;
            }
            if ($latestSingleMessage) {
                $inputParams['count'] = 1;
            }
            $originalMessageHistory     = SlackApi::execute('im.history', $inputParams);
            $messageHistory['has_more'] = $originalMessageHistory['has_more'];
            $messageHistory['messages'] = $this->slackService->prepareDirectMessageHistory(app('slackData')['slackUser'], $receiver, $originalMessageHistory);
        } catch (SlackApiException $e) {
        }

        return view('slack.partials.messageHistory')->with(compact('messageHistory', 'latestSingleMessage'));
    }

    /**
     * Fallback function, for sending message along with file upload.
     * @param MessagePost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendMessage(MessagePost $request)
    {
        $inputs     = $request->only(['receiver_id', 'message']);
        $receiverId = decrypt($inputs['receiver_id']);
        try {
            /*
             * Create or upload an existing file
             * Ref: https://api.slack.com/methods/files.upload
             */
            if ($request->hasFile('file')) {
                SlackApi::execute('files.upload', [
                    'token'           => $this->token,
                    'channels'        => $receiverId,
                    'filename'        => $request->file('file')->getClientOriginalName(),
                    'file'            => $request->file('file'),
                    'initial_comment' => $inputs['message'],
                ]);
            } else {
                /*
                 * Posts a message to a public channel, private channel, or direct message/IM channel.
                 * Ref: https://api.slack.com/methods/chat.postMessage
                 */
                SlackApi::execute('chat.postMessage', [
                    'token'   => $this->token,
                    'channel' => $receiverId,
                    'as_user' => true,
                    'text'    => $inputs['message'],
                ]);
            }
            flash('Successfully sent message!!!')->success();
        } catch (SlackApiException $e) {
            flash('Something went wrong. Try again!!!')->error();
        }

        return redirect()->route('slack.message', ['userId' => $receiverId]);
    }
}