<?php
namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use SlackApi;
use Lisennk\Laravel\SlackWebApi\Exceptions\SlackApiException;

/**
 * Class ComposerServiceProvider
 * @package App\Providers
 */
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Extending all the system views to provide the current logged in user's information
     * @param Request $request
     */
    public function boot(Request $request)
    {
        $this->app->bind('slackData', function () use ($request) {
            if ($request->session()->has('slackData')) {
                return $request->session()->get('slackData');
            } else {
                $repository = $this->app->make('App\Repositories\SlackRepository');
                $slackToken = $repository->getToken();
                $slackUser  = false;
                $users      = false;
                if ($slackToken) {
                    try {
                        /*
                         * Checks authentication and tells you who you are.
                         * Ref: https://api.slack.com/methods/auth.test
                         */
                        $authTest = SlackApi::execute('auth.test', [
                            'token' => $slackToken
                        ]);
                        /*
                         * Get information about logged in Slack User.
                         * Ref: https://api.slack.com/methods/users.info
                         */
                        $slackUser = SlackApi::execute('users.info', [
                            'token' => $slackToken,
                            'user'  => $authTest['user_id']
                        ]);
                        $slackUser = $slackUser['user'];
                        /*
                         * Get a list of all users in the team.
                         * Ref: https://api.slack.com/methods/users.list
                         */
                        $users = SlackApi::execute('users.list', [
                            'token' => $slackToken
                        ]);
                        $users = $users['members'];
                        // Store 'slackData' into session.
                        $request->session()->put('slackData', ['slackUser' => $slackUser, 'users' => $users, 'slackToken' => $slackToken]);
                    } catch (SlackApiException $e) {
                    }
                }

                return ['slackUser' => $slackUser, 'users' => $users, 'slackToken' => $slackToken];
            }
        });

        $this->app['view']->composer('layouts.navigation', function ($view) {
            $view->with('slackUser', app('slackData')['slackUser']);
            $view->with('users', app('slackData')['users']);
        });
    }
}