<?php
namespace App\Repositories;

use App\SlackTokens;

/**
 * Class SlackRepository
 * @package App\Repositories
 */
class SlackRepository
{
    /**
     * @var SlackTokens
     */
    protected $model;

    /**
     * Create new instance of Model.
     * SlackRepository constructor.
     */
    public function __construct()
    {
        $this->model = new SlackTokens();
    }

    /**
     * Get Slack Token from DB, for current authenticated user.
     * @return bool
     */
    public function getToken()
    {
        $userToken = $this->model->where('user_id', auth()->id())->first();
        if ($userToken) {
            return $userToken->token;
        }
        return false;
    }

    /**
     * Add/Update Slack Token into DB, for current authenticated user.
     * @param string $token
     * @return bool
     */
    public function storeToken($token = '')
    {
        $userToken = $this->model->where('user_id', auth()->id())->first();
        if ($userToken) {
            return $userToken->update(['token' => $token]);
        } else {
            return $this->model->fill(['user_id' => auth()->id(), 'token' => $token])->save();
        }
    }

    /**
     * Remove Slack Token from DB, for current authenticated user.
     * @throws \Exception
     */
    public function removeToken()
    {
        $this->model->where('user_id', auth()->id())->delete();
    }

}