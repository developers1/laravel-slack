<?php namespace App\Services\Socialite;

use App\Services\Socialite\Socialite;
use Laravel\Socialite\SocialiteServiceProvider as SocialiteParentServiceProvider;

class SocialiteServiceProvider extends SocialiteParentServiceProvider
{
    public function register()
    {
        $this->app->singleton('Laravel\Socialite\Contracts\Factory', function ($app) {
            return new Socialite($app);
        });
    }
}