<?php
namespace App\Services\Slack;

/**
 * Class Slack
 * @package App\Services\Slack
 */
class Slack
{
    /**
     * Slack constructor.
     */
    public function __construct()
    {
    }

    /**
     * Prepare Direct Message History in human readable format.
     * @param $sender
     * @param $receiver
     * @param $messageHistory
     * @return array|bool
     */
    public function prepareDirectMessageHistory($sender, $receiver, $messageHistory)
    {
        try {
            $messages       = [];
            $sortedMessages = array_reverse($messageHistory['messages']);

            foreach ($sortedMessages as $message) {
                if ($message['type'] == 'message' && (!isset($message['subtype']) || $message['subtype'] != 'bot_add')) {
                    $user = '';
                    $file = [];
                    // Prepare User
                    if ($message['user'] == $sender['id']) {
                        $user = $sender;
                    } else if ($message['user'] == $receiver['id']) {
                        $user = $receiver;
                    }
                    // Prepare File
                    if (isset($message['file'])) {
                        // Set file name
                        $file['name'] = $message['file']['name'];

                        // Set Preview Image
                        if (str_contains($message['file']['mimetype'], 'image')) {
                            $file['previewImg'] = $message['file']['thumb_160'];
                        }

                        // Set Preview Highlight. This will be for TEXT files.
                        if (isset($message['file']['preview_highlight'])) {
                            $file['preview_highlight'] = $message['file']['preview_highlight'];
                        }

                        // Set comments, if any posted along with file.
                        if (isset($message['file']['initial_comment'])) {
                            $file['comment'] = $message['file']['initial_comment']['comment'];
                        }

                        // Set the view/download link of file.
                        if (isset($message['file']['permalink'])) {
                            $file['permalink'] = $message['file']['permalink'];
                        }
                    }
                    // Prepare final message array
                    $returnMessage = ['user' => $user, 'text' => $message['text'], 'file' => $file, 'sent_at' => date('M d Y, h:i A', $message['ts']), 'ts' => $message['ts']];
                    $messages[]    = $returnMessage;
                }
            }
            return $messages;
        } catch (\Exception $e) {
            return false;
        }
    }
}
