<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"> <strong>WITH</strong> &nbsp;&nbsp;&nbsp;<img src="https://a.slack-edge.com/5f191/marketing/img/slack_logo.png"></p>


## About this App

This is a Web Application, that integrates Slack API with Laravel, providing following facilities:

- [Authentication using Laravel Auth](https://laravel.com/docs/5.4/authentication)
- [Authentication into Slack](https://api.slack.com/docs/oauth)
- [Listing all Slack Users into App, using Slack Web API](https://api.slack.com/web).
- [Opens Direct Message Channel with Slack, to send message (simple text + single file upload) to Slack User, using methods of Slack Web API](https://api.slack.com/methods)
- [Retrive and show Message History, done with Slack User, using methods of Slack Web API](https://api.slack.com/methods)


## Setup Slack App

- Setup your Slack Team, Invite Memebers.
- Create/Update your Slack App from https://api.slack.com/apps
- Setup App credentials from **Basic Information** section. Ref: https://www.dropbox.com/s/k4mlnvmxx0zk543/App%20Credentials.png?dl=0
- Activate Incoming Webhooks from **Add features and functionality -> Incoming Webhooks** section. Ref: https://www.dropbox.com/s/m7qqhlfv6fwr9zz/Activate%20Incoming%20Webhooks.png?dl=0
- Setup Auth Redirect URLs, Permission Scopes from **Add features and functionality -> OAuth & Permissions** section. https://www.dropbox.com/s/2tcbcpsjnrrqkal/Redirect%20URL.png?dl=0


## Setup in Local

- Clone the repo.
- Change **.env** file with proper settings for App + DB.
- Run: composer update
- Run: php artsian migrate
- Run: php artisan db:seed
- Add below settings for Slack in **.env**
  - SLACK_CLIENT_ID=YOUR SLACK APP'S CLIENT ID
  - SLACK_CLIENT_SECRET=YOUR SLACK APP'S CLIENT SECRET
  - SLACK_REDIRECT_CALLBACK_URL=YOUR SLACK APP'S **Auth Redirect URL** SET UP IN **Add features and functionality -> OAuth & Permissions** SECTION
- And you are ready to go!!! Run your app, login with "remo@email.com(secret)" OR "john@email.com(secret)" into App.
- Get into Slack, from Navigation after Login. Ref: https://www.dropbox.com/s/dp4kkxw3llwvmlj/Get%20In%20Slack.png?dl=0
