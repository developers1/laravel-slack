<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'slack', 'middleware' => ['setSlackToken']], function () {
        Route::get('/', 'SlackController@index')->name('slack.index');
        Route::get('auth', 'SlackController@auth')->name('slack.auth');
        Route::get('auth/callback', 'SlackController@authCallback')->name('slack.auth.callback');
        Route::get('message/{userId}', 'SlackController@message')->name('slack.message');
        Route::get('ajax/imMessageHistory/{userId}/{imChannelId}/{latestMessageTS?}/{latestSingleMessage?}', 'SlackController@imMessageHistory')->name('slack.ajax.imMessageHistory');
        Route::post('message', 'SlackController@sendMessage')->name('slack.sendMessage');
    });
});

Route::get('/', function () {
    return redirect()->route('slack.index');
});