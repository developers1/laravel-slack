@extends('layouts.layout')
@section('title')
    {{ $receiver['name'] }} |
@endsection

@section('content')

    <div class="row send-message-page">
        <!-- Page Heading -->
        <div class="col-lg-12 heading-area">
            <h1 class="page-header">
                <small>{{ '@'.$receiver['name'] }}</small>
            </h1>
        </div>

        <div class="content-area">
            <!-- RECEIVER INFO -->
            <div class="col-lg-12 receiver-info">
                <div class="media">
                <span class="pull-left">
                    <img class="media-object" src="{{$receiver['profile']['image_72']}}" alt="{{$receiver['name']}}">
                </span>
                    <div class="media-body">
                        <h5 class="media-heading"><strong>{{$receiver['profile']['real_name']}}&nbsp;<i class="fa fa-circle" style="color: {{'#'.$receiver['color']}}"></i></strong>
                        </h5>
                        <p class="small text-muted">{{'@'.$receiver['name']}}</p>
                    </div>
                </div>
            </div>

            <!-- MESSAGE HISTORY -->
            <div class="col-lg-12 message-history-area">
                <div class="message-history">
                    @include('slack.partials.messageHistory')
                </div>
            </div>
        </div>

        <!-- SEND MESSAGE -->
        <div class="col-lg-12 send-message-area">
            {!! Form::open(['route' => 'slack.sendMessage', 'id'=>'sendMessage', 'files' => true]) !!}
            {!! Form::hidden('receiver_id', encrypt($receiver['id'])) !!}
            <div class="row">
                <div class="col-lg-6 form-group">
                    {!! Form::text('message', '', ['class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Message @'.$receiver['name']]) !!}
                </div>
                <div class="col-lg-5 form-group">
                    {!! Form::file('file', ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary" id="btn-send-message">Send</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section("page_js")
    <script type="text/javascript">

        var DirectMessage = function () {
            var token               = '{{app('slackData')['slackToken']}}';
            var channel             = '{{$receiver['id']}}';
            var imMessageHistoryURL = '{{route('slack.ajax.imMessageHistory', ['userId' => $receiver['id'], '' => $imChannelId])}}';

            var scrollToLastMessage = function () {
                $('.content-area').animate({
                    scrollTop: $('.message-history .media:last').offset().top
                }, 'slow');
            };

            var sendMessage = function () {
                $('form#sendMessage').submit(function (e) {
                    e.preventDefault();
                    var url             = 'https://slack.com/api/chat.postMessage';
                    var file            = $('input:file')[0].files[0];
                    var initial_comment = $('input[name="message"]').val();
                    var form_data       = new FormData();
                    form_data.append('token', token);

                    if (typeof file != 'undefined') {
                        url = 'https://slack.com/api/files.upload';
                        form_data.append('channels', channel);
                        form_data.append('file', file);
                        form_data.append('initial_comment', initial_comment);
                    } else {
                        form_data.append('channel', channel);
                        form_data.append('as_user', true);
                        form_data.append('text', initial_comment);
                    }

                    $.ajax({
                        url        : url,
                        method     : 'POST',
                        contentType: false,
                        processData: false,
                        cache      : false,
                        data       : form_data,
                    }).done(function (data) {
                        var msg      = 'Something went wrong. Try again!!!';
                        var priority = 'danger';
                        if (data.ok) {
                            msg      = 'Successfully sent message!!!';
                            priority = 'success';

                            $.ajax({
                                url    : imMessageHistoryURL + '/0/1',
                                type   : 'GET',
                                success: function (response) {
                                    $('div.message-history').append(response);
                                    scrollToLastMessage();
                                }
                            })
                        }
                        $.toaster({message: msg, timeout: 3000, priority: priority});

                        $('form#sendMessage')[0].reset();
                    });
                });
            };

            var loadAjaxMessageHistory = function () {
                $('.content-area').scroll(function () { //detact scroll
                    if ($(this).scrollTop() == 0 && $('input[name="hasMoreMessages"]').val() == 1) {
                        var hasMoreMessages = $('input[name="hasMoreMessages"]').val();
                        var latestMessageTS = $('input[name="latestMessageTS"]').val();
                        $.ajax({
                            url    : imMessageHistoryURL + '/' + latestMessageTS,
                            type   : 'GET',
                            success: function (response) {
                                $('input[name="hasMoreMessages"], input[name="latestMessageTS"]').remove();
                                $('div.message-history').prepend(response);
                            }
                        })
                    }
                });
            };

            return {
                init: function () {
                    scrollToLastMessage();
                    sendMessage();
                    loadAjaxMessageHistory();
                }
            }
        }();

        $(document).ready(function () {
            DirectMessage.init();
        });
    </script>
@endsection