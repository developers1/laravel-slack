@foreach($messageHistory['messages'] as $message)
    <div class="media">
            <span class="pull-left">
                <img class="media-object" src="{{$message['user']['profile']['image_32']}}" alt="{{$message['user']['name']}}">
            </span>
        <div class="media-body">
            <h5 class="media-heading">
                <strong>{{$message['user']['profile']['real_name']}}</strong><span class="small text-muted">&nbsp;{{ $message['sent_at'] }}</span>
            </h5>

            @if($message['file'])
                <p> Uploaded:
                @if(isset($message['file']['permalink']))
                    <a href="{{$message['file']['permalink']}}" target="_blank">
                        {!! $message['file']['name'] !!}
                    </a>
                @else
                    {!! $message['file']['name'] !!}
                @endif
                </p>

                @if(isset($message['file']['preview_highlight']))
                    {!! $message['file']['preview_highlight'] !!}
                @endif

                @if(isset($message['file']['previewImg']))
                    <p><img src="{{$message['file']['previewImg']}}"></p>
                @endif

                @if(isset($message['file']['comment']))
                    <p class="small">{!! $message['file']['comment'] !!}</p>
                @endif
            @else
                <p class="small">{!! $message['text'] !!}</p>
            @endif

        </div>
    </div>
@endforeach
@if(!isset($latestSingleMessage) || (!$latestSingleMessage))
{{Form::hidden('hasMoreMessages', $messageHistory['has_more'])}}
{{Form::hidden('latestMessageTS', ($messageHistory['messages'] ? $messageHistory['messages'][0]['ts'] : 0))}}
@endif