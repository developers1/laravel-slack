@if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->getMessages() as $this_error)
            <button class="close" data-dismiss="alert" type="button">×</button>{{ $this_error[0] }}<br/>
        @endforeach
    </div>
@endif