<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Slack App</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        @if (!Auth::guest())
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> App <b class="caret"></b></a>--}}
                <ul class="dropdown-menu">
                    @if($slackUser)
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="{{$slackUser['profile']['image_48']}}" alt="{{$slackUser['name']}}">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>{{$slackUser['profile']['real_name']}}&nbsp;<i class="fa fa-circle" style="color: {{'#'.$slackUser['color']}}"></i></strong>
                                        </h5>
                                        <p class="small text-muted">{{'@'.$slackUser['name']}}</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('slack.auth') }}"><i class="fa fa-fw fa-user"></i> Get in Slack!</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        @endif
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            @if($users)
                <li class="active">
                    <a><i class="fa fa-comment"></i> Direct Messages</a>
                    <ul>
                        @foreach($users as $user)
                            @php $class = '' @endphp
                            @if(\Request::route()->getName() == 'slack.message' && \Request::route()->parameter('userId') == $user['id'])
                                @php $class = 'active' @endphp
                            @endif
                            <li class="{{$class}}" >
                                <a href="{{ route('slack.message', ['userId' => $user['id']]) }}"><i class="fa fa-circle" style="color: {{'#'.$user['color']}}"></i>&nbsp;{{$user['profile']['real_name']}}@if($slackUser && $slackUser['id'] == $user['id']) (you) @endif</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @else
            <li>
                <a href="{{ route('slack.index') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            @endif
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>