<!-- jQuery -->
{!! Html::script('js/jquery.js') !!}

<!-- Bootstrap Core JavaScript -->
{!! Html::script('js/bootstrap.min.js') !!}

<!-- Bootstrap Toaster -->
{!! Html::script('js/jquery.toaster.js') !!}

<!-- Pace -->
{!! Html::script('js/pace.min.js') !!}