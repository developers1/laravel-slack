<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title') Slack with Laravel App</title>

    <!-- Bootstrap Core CSS -->
    {!! Html::style('css/bootstrap.min.css') !!}

    <!-- Custom CSS -->
    {!! Html::style('css/sb-admin.css') !!}

    <!-- Morris Charts CSS -->
    {!! Html::style('css/plugins/morris.css') !!}

    <!-- Custom Fonts -->
    {!! Html::style('font-awesome/css/font-awesome.min.css') !!}

    <!-- Pace -->
    {!! Html::style('css/pace.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        paceOptions = {
            ajax: true, // disabled
            document: true, // disabled
            eventLag: false, // disabled
            /*elements: {
                selectors: ['.my-page']
            }*/
        };
    </script>
</head>