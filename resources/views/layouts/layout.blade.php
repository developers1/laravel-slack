<!DOCTYPE html>
<html lang="en">

<!-- Header -->
@include('layouts.head')

<body>

<div id="wrapper">

    <!-- Navigation -->
    @include('layouts.navigation')

    <div id="page-wrapper">
        <div class="container-fluid">

            <!-- Error Notifications -->
            <div class="row">
                <div class="col-lg-12 page-notification">
                    @include('errors')
                </div>
            </div>

            <!-- Flash Notifications -->
            <div class="row">
                <div class="col-lg-12 page-notification">
                    @include('flash::message')
                </div>
            </div>

            <!-- Content -->
            @yield('content')
        </div>
    </div>

</div>

<!-- Footer -->
@include('layouts.foot')

@yield('page_js')

</body>

</html>
